#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""A module that refreshes this plugin's contents."""
import itertools

import os
import textwrap
from pxr import Plug

_CURRENT_DIRECTORY = os.path.dirname(os.path.realpath(__file__))


def get_syntax_lines():
    """Run the main execution of the current script."""

    def _get_first_letter(name):
        return name[0].lower()

    template = "{name}	/	language:{language}"
    lines = []

    for name in get_type_keywords():
        lines.append(template.format(name=name, language="usd"))
        lines.append(template.format(name=name, language="usda"))

    return lines


def get_type_keywords():
    """set[str]: Get every known USD Type that can be written into a USD file."""

    def _is_reserved(name):
        return name.startswith("__")

    all_types = set()

    for plugin in Plug.Registry().GetAllPlugins():
        for key, value in (plugin.metadata.get("Types") or dict()).items():
            type_name = value.get("primTypeName")

            if type_name and not _is_reserved(type_name):
                all_types.add(type_name)

    return all_types


def main():
    """Run the main execution of the current script."""
    usd_syntax_file = os.path.normpath(
        os.path.join(_CURRENT_DIRECTORY, os.pardir, "tags", "usd-tags")
    )
    readme_file = os.path.normpath(
        os.path.join(_CURRENT_DIRECTORY, os.pardir, "tags", "README.md")
    )

    directory = os.path.dirname(usd_syntax_file)

    if not os.path.isdir(directory):
        os.makedirs(directory)

    with open(readme_file, "w") as handler:
        handler.write(
            textwrap.dedent(
                """\
                The "{usd_syntax_file}" is auto-generated by usd_schema_generator.py
                To update this list, re-run usd_schema_generator.py in your USD environment
                """
            ).format(usd_syntax_file=usd_syntax_file)
         )

    with open(usd_syntax_file, "w") as handler:
        handler.writelines("\n".join(sorted(get_syntax_lines())))


if __name__ == "__main__":
    main()
